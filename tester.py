#!/usr/bin/env python3
import os
import re
import subprocess
import time
from telegram import Bot
import emoji
import urllib.request

with open('token.txt','r') as file:
    TOKEN = file.readline().strip()
bot = Bot(TOKEN)

response = subprocess.Popen('python3 speedtest-cli --server 3221  --simple --share', shell=True, stdout=subprocess.PIPE).stdout.read().decode('utf-8')

ping = re.findall('Ping:\s(.*?)\s', response, re.MULTILINE)
download = re.findall('Download:\s(.*?)\s', response, re.MULTILINE)
upload = re.findall('Upload:\s(.*?)\s', response, re.MULTILINE)
share = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+',response,re.MULTILINE)
bad_chars=["[","]","'"]
for i in bad_chars :
    url = str(share).replace(i, '')
urllib.request.urlretrieve(str(url)[1:-1], "pictures/latest.png")

ping = ping[0].replace(',', '.')
download = download[0].replace(',', '.')
upload = upload[0].replace(',', '.')

try:
    f = open('csv/test.csv', 'a+')
    if os.stat('csv/test.csv').st_size == 0:
            f.write('Date|Time|Ping (ms)|Download (Mbit/s)|Upload (Mbit/s)\r\n')
except:
    pass

f.write('{}|{}|{}|{}|{}\r\n'.format(time.strftime('%m/%d/%y'), time.strftime('%H:%M'), ping, download, upload))

#Replace with appropiate number depending on what speed rate you need the bot to notify you
if float(download) < 125:
    #Replace 1111111 with your chat id
	   bot.sendPhoto(chat_id='1111111',photo=str(url)[1:-1],caption= emoji.emojize(':rocket: Test automático por baja velocidad'))
