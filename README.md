A set of scripts that lets you use speedtest-cli with a Telegram bot using https://python-telegram-bot.org/



Some of the code I got it from here https://pimylifeup.com/raspberry-pi-internet-speed-monitor/ so if you need a better explanation of how it works.

The scripts work fine, but there are things that could be better implemented. (like adding a thread for when the bot is sending the picture)

Replace the server ID with the one you need on https://gitlab.com/LinuxGhost/spdtestes/-/blob/master/spdlistener3.py#L50  and https://gitlab.com/LinuxGhost/spdtestes/-/blob/master/tester.py#L14

For Google Drive sync you could follow this tutorial http://www.linuxandubuntu.com/home/google-drive-cli-client-for-linux

