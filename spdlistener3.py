#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from emoji import emojize
import re
import logging
import argparse
from functools import wraps
from telegram import ReplyKeyboardMarkup,ChatAction
from telegram.ext import Updater,CommandHandler,Filters,MessageHandler

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

keyboard = ReplyKeyboardMarkup(keyboard=[[emojize(':rocket: Test Velocidad')]])
#Replace with the chat id/s you want the bot to have access 
id_a = [111111111,22222222,333333,44444]

def send_action(action):
    """Sends `action` while processing func command."""

    def decorator(func):
        @wraps(func)
        def command_func(update, context, *args, **kwargs):
            context.bot.send_chat_action(chat_id=update.effective_message.chat_id, action=action)
            return func(update, context,  *args, **kwargs)
        return command_func

    return decorator

send_upload_photo_action = send_action(ChatAction.UPLOAD_PHOTO)

def echo(bot, context):
    """Echo the user message."""
    bot.message.reply_text(bot.message.text)
    inf = bot.message.from_user
    command = bot.message.text

@send_upload_photo_action
def start(bot,context):
    sender = bot.message.chat_id
    inf = bot.message.from_user
    command = bot.message.text
    print ('Recibi '+ command)
    if sender in id_a :
      if command == '/start':
       bot.message.reply_text('Bienvenido',reply_markup=keyboard)
      elif command == '🚀 Test Velocidad':
       #Change the server ID closer to your location
       rel = os.popen("python3 speedtest-cli --server 3221  --simple --share").read()
       url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', rel)
       context.bot.send_chat_action(sender,'upload_photo')
       bad_chars=["[","]","'"]
       for i in bad_chars :
         	url = str(url).replace(i, '')
       bot.message.reply_photo(photo=url,caption=emojize(':rocket: Ultimo test'),reply_markup=keyboard)
      else:
          #Send a funny Trump sticker every time you issue a non command, you can replace te sticker with your own, just change the ID
          bot.message.reply_sticker('CAADAgADEgMAArVx2gZFPmlPLcP4cBYE')
    else:
      bot.message.reply_text('This is a private bot')

def error(bot,context):
    logger.warning('Update "%s" caused error "%s"', bot, context.error)
    print(logger.warning)

def main(TOKEN):
    with open(TOKEN, 'r') as file:
        api_key = file.readline().strip()
    updater = Updater(api_key,use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(MessageHandler(Filters.text, start))
    dp.add_error_handler(error)
    updater.start_polling()
    updater.idle()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--key_path', help='Path to the file containing the api key, by default will use token.txt in the same directory', default='token.txt')
    args = parser.parse_args()
    main(args.key_path)